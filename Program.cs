﻿namespace ToDoTwo {
	class Program {
		static string duedate() {
			string due = "undefined";
			while (true) {
				Console.Write("Does this task have a duedate? ");
				string yesno = Console.ReadLine();
				yesno = yesno.ToLower();
				if (yesno == "yes") {
					Console.Write("When is the duedate? ");
					due = Console.ReadLine();
					break;
				}
				else if (yesno == "no") {
					break;
				}
				else {
					Console.WriteLine("Invalid option; please either type 'yes' or 'no'.");
				}
			}
			return due;
		}
		static void Main(string[] args) {
			Console.Title = "To-Do (improved)";
            List<string> duelist = new List<string>();
			string total = "";
			Dictionary<string, List<string>> userlist = new Dictionary<string, List<string>>();
			Console.WriteLine("Welcome to my To-Do list maker! Please press Ctrl+C to quit, and type 'help' for a list of available functions.");
			while (true) {
				try {
					Console.Write("Command$ ");
					string command = Console.ReadLine();
					command = command.ToLower();
					if (command == "add") {
						Console.Write("What would you like to call your list? ");
						string listname = Console.ReadLine();
						userlist[listname] = new List<string>();
						Console.Write("How many items would you like to add to your list? ");
						int answer = Convert.ToInt32(Console.ReadLine());
						if (answer <= 0 || answer > 100) {
							Console.WriteLine("Invalid. Number. Please retry.");
							continue;
						}
						else {
						for (int i = 0; i < answer; i++) {
							int tally = i + 1;
							switch(tally) {
								case 1:
								case 11:
								case 21:
								case 31:
								case 41:
								case 51:
								case 61:
								case 71:
								case 81:
								case 91:
									Console.Write("What is your " + tally + "st task? ");
									userlist[listname].Add(Console.ReadLine());
									duelist.Add(duedate());
									break;
								case 2:
								case 12:
								case 22:
								case 32:
								case 42:
								case 52:
								case 62:
								case 72:
								case 82:
								case 92:
									Console.Write("What is your " + tally + "nd task? ");
									userlist[listname].Add(Console.ReadLine());
									duelist.Add(duedate());
									break;
								case 3:
								case 13:
								case 23:
								case 33:
								case 43:
								case 53:
								case 63:
								case 73:
								case 83:
								case 93:
									Console.Write("What is your " + tally + "rd task? ");
									userlist[listname].Add(Console.ReadLine());
									duelist.Add(duedate());
									break;
								default:
									Console.Write("What is your " + tally + "th task? ");
									userlist[listname].Add(Console.ReadLine());
									duelist.Add(duedate());
									break;
									
								}
							total = total + listname + ":\n" + tally + ". " + userlist[listname][i] + " : " + duelist[i] + "\n";
							}
						Console.WriteLine(total);
						total = "";
						}
					}
					else if (command == "print") {
						Console.Write("Which list would you like to print? ");
						string listname = Console.ReadLine();
						int index = 0;
						foreach (var item in userlist[listname]) {
							index++;
							Console.WriteLine("{0}. {1}", index, item);
						}
					}
					else if (command == "edit") {
						Console.Write("Which list would you like to edit? ");
						string listname = Console.ReadLine();
						Console.Write("Which entry would you like to change? ");
						int listnum = Convert.ToInt32(Console.ReadLine());
						Console.Write("What would you like to change it to? ");
						userlist[listname][listnum - 1] = Console.ReadLine();
					}
					else if (command == "help") {
						Console.WriteLine("1. Add: creates a list. Pretty self explanatory.\n2. Print: allows you to print out an already existing list.\n3. Edit: Allows you to edit an entry in an already existing list.\n4. Help: Well... I guess now you know.\n5. Quit: gets you back to your regular shell.");
					}
					else if (command == "quit" || command == "exit" || command == "bye") {
						Console.WriteLine("Byebye!!");
						break;
					}
					else {
						Console.WriteLine("Command not recognized. Please retry.");
					}
				}
				catch (FormatException) {
					Console.WriteLine("Error: You did something wrong with your formatting. (Did you put a letter in place of a number/vice versa?) Please retry.");
				}
				catch (KeyNotFoundException) {
					Console.WriteLine("Error: That list name was invalid. Please retry.");
				}
			}
		}
	}
}
